/*
 * File:   main.c
 * Author: Admin
 *
 * Created on 23 agosto 2014, 21.35
 */

#define _SUPPRESS_PLIB_WARNING 1
#include <plib.h>

#define MIN_MOTOR_DUTY      0x270F  // 1 msec
#define MAX_MOTOR_DUTY      0x4E1E  // 2 msec
#define PERIODREGISTER2     0xC34F  // 5 msec

#define UART_MODULE_ID UART2 // PIM is connected to Explorer through UART2 module
#define DESIRED_BAUDRATE (57600) //The desired BaudRate

#pragma config FPLLMUL = MUL_20, FPLLIDIV = DIV_2, FPLLODIV = DIV_1, FWDTEN = OFF
#pragma config FPBDIV = DIV_1
#pragma config FNOSC = FRCPLL       // Internal Fast RC oscillator (8 MHz) w/ PLL
#pragma config FSOSCEN = OFF        // Disable secondary oscillator
#define SYS_FREQ (80000000L)

#define	GetPeripheralClock()		(SYS_FREQ/(1 << OSCCONbits.PBDIV))
#define	GetInstructionClock()		(SYS_FREQ)

/*
 *
 */
int BLmotor[4];
int BLcount;

// Function Prototypes
void SendDataBuffer(const char *buffer, UINT32 size);
UINT32 GetMenuChoice(void);
UINT32 GetDataBuffer(char *buffer, UINT32 max_size);

void initializePWM(void);
void initializeUART(void);

// Constant Data
const char mainMenu[] =
{
    "Welcome to Brushless motor controller! \r\n"\
    "Here are the main menu choices \r\n"\
    "1. ESC initialization \r\n"\
    "2. Set PWM Duty Cycle (0: 1 msec - 99: 2 msec \r\n"\
    "3. Manual motor control \r\n"\

    "\r\n\r\nPlease Choose a number\r\n"
};

int main(int argc, char** argv)
{
    UINT32 requiredPWM = 0;
    UINT32  menu_choice;
    UINT8   buf[1024];
    // Configure the proper PB frequency and the number of wait states
    SYSTEMConfigPerformance(SYS_FREQ);

    INTEnableSystemMultiVectoredInt();

    int counter;

    initializeUART();
    initializePWM();
    
    SendDataBuffer(mainMenu, sizeof(mainMenu));
    while (1)
    {
        menu_choice = GetMenuChoice();

        switch(menu_choice)
        {
        case '1':
            //sprintf(buf, "Actual Baud Rate: %ld\r\n\r\n", UARTGetDataRate(UART_MODULE_ID, GetPeripheralClock()));
            sprintf(buf, "***Disconnect ESC power\r\n"\
                         "***then press ENTER\r\n\r\n");
            SendDataBuffer(buf, strlen(buf));
            menu_choice = GetMenuChoice();
            BLmotor[0] = MAX_MOTOR_DUTY;

            sprintf(buf, "***Connect ESC Power\r\n"\
                         "***Wait beep-beep\r\n"\
                         "***then press ENTER\r\n\r\n");
            SendDataBuffer(buf, strlen(buf));
            menu_choice = GetMenuChoice();
            BLmotor[0] = MIN_MOTOR_DUTY;

            sprintf(buf, "***Wait N beep for battery cell\r\n"\
                         "***Wait beeeeeep for ready\r\n"\
                         "***then press ENTER\r\n\r\n");
            SendDataBuffer(buf, strlen(buf));
            break;

        case '2':
        {
            UINT32 newRequiredPWM = 0;
            int data_size = 0;
            sprintf(buf, "***Insert Duty Cycle value: \r\n");
            SendDataBuffer(buf, strlen(buf));
            data_size = GetDataBuffer(buf, 4);  /* Get PWM from user */
            newRequiredPWM = atoi(buf); /* Convert string PMW value to int type */
            if ((newRequiredPWM >= 0) && (newRequiredPWM <= 100))
            {
                requiredPWM = newRequiredPWM;
                BLmotor[0] = (((MAX_MOTOR_DUTY - MIN_MOTOR_DUTY) / 100 * requiredPWM) + MIN_MOTOR_DUTY);
            }
            sprintf(buf, "***Duty Cycle value: %d\r\n"\
                         "***New DCOC1PWM value: %d\r\n\r\n",\
                         newRequiredPWM, BLmotor[0]);
            SendDataBuffer(buf, strlen(buf));

            break;
        }

        case '3':
        {
            UINT32 dataEntered = 0;
            sprintf(buf, "***Press a to increase motor speed\r\n"\
                         "***Press z to decrease motor speed\r\n");
            SendDataBuffer(buf, strlen(buf));
            sprintf(buf, "***Previous Duty Cycle value: %d --- "\
                         "previous DCOC1PWM value: %d\r\n",\
                         requiredPWM, BLmotor[0]);
            SendDataBuffer(buf, strlen(buf));
            dataEntered = GetMenuChoice();
            switch (dataEntered)
            {
                case 'a':
                    if (requiredPWM < 100)
                    {
                        requiredPWM++;
                        BLmotor[0] = (((MAX_MOTOR_DUTY - MIN_MOTOR_DUTY) / 100 * requiredPWM) + MIN_MOTOR_DUTY);
                    }
                    break;

                case 'z':
                    if (requiredPWM > 0)
                    {
                        requiredPWM--;
                        BLmotor[0] = (((MAX_MOTOR_DUTY - MIN_MOTOR_DUTY) / 100 * requiredPWM) + MIN_MOTOR_DUTY);
                    }
                    break;
            }
            sprintf(buf, "***New Duty Cycle value: %d --- "\
                         "***new DCOC1PWM value: %d\r\n\r\n",\
                         requiredPWM, BLmotor[0]);
            SendDataBuffer(buf, strlen(buf));
        }
        
        default:
            SendDataBuffer(mainMenu, sizeof(mainMenu));

        }
    }
    CloseTimer2();
    CloseOC1();
    CloseOC2();
    CloseOC3();
    CloseOC4();
}


// *****************************************************************************
// PWM initialization
// *****************************************************************************
void initializePWM(void)
{
    // init the output compare modules
    OpenOC1( OC_ON | OC_TIMER2_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    OpenOC2( OC_ON | OC_TIMER2_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    OpenOC3( OC_ON | OC_TIMER2_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);
    OpenOC4( OC_ON | OC_TIMER2_SRC | OC_PWM_FAULT_PIN_DISABLE, 0, 0);

    // init Timer2 mode and period (PR2)
    // Fpb = SYS_FREQ = 80Mhz (From configuration)
    // Timer Prescale = 8
    // PR2 = 0x9C3F = 49,999
    // interrupts every 4 ms
    // 5 ms = (PR2 + 1) * TMR Prescale / Fpb = (39999 + 1) * 8 / 80000000
    CloseTimer2();
    OpenTimer2 (T2_ON | T2_PS_1_8 | T2_SOURCE_INT, 0xC34F);
    ConfigIntTimer2(T2_INT_ON | T2_INT_PRIOR_7);

    mT2SetIntPriority(7);   // set Timer2 Interrupt Priority
    mT2ClearIntFlag();      // clear interrupt flag
    mT2IntEnable(1);        // enable timer2 interrupts
}


// *****************************************************************************
// UART initialization
// *****************************************************************************
void initializeUART(void)
{
    UARTConfigure(UART_MODULE_ID, UART_ENABLE_PINS_TX_RX_ONLY);
    UARTSetFifoMode(UART_MODULE_ID, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
    UARTSetLineControl(UART_MODULE_ID, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
    UARTSetDataRate(UART_MODULE_ID, GetPeripheralClock(), 57600);
    UARTEnable(UART_MODULE_ID, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));
}

// *****************************************************************************
// void UARTTxBuffer(char *buffer, UINT32 size)
// *****************************************************************************
void SendDataBuffer(const char *buffer, UINT32 size)
{
    while(size)
    {
        while(!UARTTransmitterIsReady(UART_MODULE_ID))
            ;

        UARTSendDataByte(UART_MODULE_ID, *buffer);

        buffer++;
        size--;
    }

    while(!UARTTransmissionHasCompleted(UART_MODULE_ID))
        ;
}
// *****************************************************************************
// UINT32 GetDataBuffer(char *buffer, UINT32 max_size)
// *****************************************************************************
UINT32 GetDataBuffer(char *buffer, UINT32 max_size)
{
    UINT32 num_char;

    num_char = 0;

    while(num_char < max_size)
    {
        UINT8 character;

        while(!UARTReceivedDataIsAvailable(UART_MODULE_ID))
            ;

        character = UARTGetDataByte(UART_MODULE_ID);

        if(character == '\r')
            break;

        *buffer = character;

        buffer++;
        num_char++;
    }

    return num_char;
}
// *****************************************************************************
// UINT32 GetMenuChoice(void)
// *****************************************************************************
UINT32 GetMenuChoice(void)
{
    UINT8  menu_item;

    while(!UARTReceivedDataIsAvailable(UART_MODULE_ID))
        ;

    menu_item = UARTGetDataByte(UART_MODULE_ID);

    //menu_item -= '0';

    return (UINT32)menu_item;
}

void __ISR( _TIMER_2_VECTOR, ipl7) T2Interrupt( void)
{
    if (++BLcount >= 4)
    {
        BLcount = 0;
    }
    // Set PWM pin low
    SetDCOC1PWM(1);

    switch(BLcount)
    {
        case 0:
            SetDCOC1PWM(BLmotor[BLcount]);
            break;
        case 1:
            SetDCOC2PWM(BLmotor[BLcount]);
            break;
        case 2:
            SetDCOC3PWM(BLmotor[BLcount]);
            break;
        case 3:
            SetDCOC4PWM(BLmotor[BLcount]);
            break;
    }
    // clear interrupt flag and exit
    mT2ClearIntFlag();
}   // T2 Interrupt

